<?php

/**
 * @file
 * Admin page callbacks for the parallel css module.
 */
function parallel_css_admin_form(&$form_state) {
 $description = <<<EOT
  <p>Enter the domains urls you want included separated by each line. Warning dont include a '/' at the end of the domain url.</p>
  <ul style="list-style: none;">For example
    <li>http://img1.drupal.org</li>
    <li>http://img2.drupal.org</li>
    <li>http://img3.drupal.org</li>
    <li>http://img4.drupal.org</li>
    <li>https://s1.amazonaws.com/drupal_cdn</li>
  </ul>
  <p> In addition for SEO purposes (prevent double content) : Please update the .htaccess file </p>
  <p>In between these two lines: </p>
  <p># RewriteBase / </p>
  <p># Rewrite URLs of the form 'x' to the form 'index.php?q=x'. </p>

<ul style="list-style: none;"># Parallel CSS - Start
<li>RewriteCond %{HTTP_HOST} img1.drupal.org [NC] </li>
<li>RewriteCond %{REQUEST_URI} !\.(png|gif|jpg|jpeg|ico)$ [NC] </li>
<li>RewriteRule ^(.*)$ http://www.drupal.org/$1 [L,R=301] </li>
<li>&nbsp</li>
<li>RewriteCond %{HTTP_HOST} img2.drupal.org [NC] </li>
<li>RewriteCond %{REQUEST_URI} !\.(png|gif|jpg|jpeg|ico)$ [NC] </li>
<li>RewriteRule ^(.*)$ http://www.drupal.org/$1 [L,R=301] </li>
<li>&nbsp</li>
<li>RewriteCond %{HTTP_HOST} img3.drupal.org [NC] </li>
<li>RewriteCond %{REQUEST_URI} !\.(png|gif|jpg|jpeg|ico)$ [NC] </li>
<li>RewriteRule ^(.*)$ http://www.drupal.org/$1 [L,R=301] </li>
<li>&nbsp</li>
<li>RewriteCond %{HTTP_HOST} img4.drupal.org [NC] </li>
<li>RewriteCond %{REQUEST_URI} !\.(png|gif|jpg|jpeg|ico)$ [NC] </li>
<li>RewriteRule ^(.*)$ http://www.drupal.org/$1 [L,R=301] </li>
<li>&nbsp</li>
<li>RewriteCond %{HTTP_HOST} s1.amazonaws.com/drupal_cdn [NC] </li>
<li>RewriteCond %{REQUEST_URI} !\.(png|gif|jpg|jpeg|ico)$ [NC] </li>
<li>RewriteRule ^(.*)$ http://www.drupal.org/$1 [L,R=301] </li>
# Parallel CSS - End </ul>

EOT;
  $form['parallel_css_settings'] = array(
    '#type' => 'textarea',
    '#title' => t('URL'),
    '#description' => t($description),
    '#default_value' => variable_get('parallel_css_settings', ''),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}